import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author aadityaparashar
 */
public class GraphTaskTest {

   /**
    * Runs the basic test, to see if the program creates a new Graph, fills it with vertices and makes edges,
    * and then print the graph, and then call the floydWarshall method which will find the longest distance between each pair of vertices
    */
   @Test (timeout=20000)
   public void CoreFunctionTest() {

      GraphTask.Graph g = new GraphTask.Graph("G");
      g.createRandomSimpleGraph(6, 9, false);
      System.out.println(g);
      g.floydWarshall();
   }

   /**
    * Runs basic commands, to test if it creates Graph, fills it and runs the floydWarshall method.
    * However, the fill graph, should give an error, as the number of edges are not possible with the given number of vertices
    * @exception IllegalArgumentException
    */
   @Test(expected =  IllegalArgumentException.class, timeout=20000)
   public void IllegalArgument() {

      GraphTask.Graph t = new GraphTask.Graph("Test Graph");
      t.createRandomSimpleGraph (6, 29,true);
      t.floydWarshall();

   }

   /**
    * Creates new Graph, fills it with vertices and edges.
    * Then prints the distance matrix. To test that the generation is okay
    */
   @Test (timeout = 20000)
   public void generateDistanceMatrix(){
      GraphTask.Graph g = new GraphTask.Graph("Test 3");
      g.createRandomSimpleGraph(6,8,false);
      g.printMapMatrix();

   }

   /**
    * Multiple times runs the new Graph generation
    * and then creates the random graoh with small number of vertices (less than 20)
    * and then runs the floydWarshall funtion on each of the graphs.
    */
   @Test
   public void smallSetFloydWarshall(){

      GraphTask.Graph g = new GraphTask.Graph("Test 3");
      g.createRandomSimpleGraph(6,8,false);
      g.floydWarshall();

      GraphTask.Graph h = new GraphTask.Graph("Test 3");
      h.createRandomSimpleGraph(16,18,true);
      h.floydWarshall();

   }

   /**
    * Multiple times runs the new Graph generation
    * and then creates the random graoh with large number of vertices (once with 16, once with 2499 vertices)
    * and then runs the floydWarshall funtion on each of the graphs.
    */
   @Test
   public void largeSetFloydWarshall(){


      GraphTask.Graph j = new GraphTask.Graph("Test 3");
      j.createRandomSimpleGraph(16,18,false);
      j.floydWarshall();

      GraphTask.Graph k = new GraphTask.Graph("Test 3");
      k.createRandomSimpleGraph(2499,2499,false);
      k.floydWarshall();

   }



}

