import java.util.*;
import java.lang.*;
import java.io.*;

/**
References:
- https://www.baeldung.com/java-dijkstra
- https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-in-java-using-priorityqueue/
- https://enos.itcollege.ee/~jpoial/algorithms/graphs1.html
- https://en.wikipedia.org/wiki/Floyd-Warshall_algorithm
- https://algs4.cs.princeton.edu/44sp/
- https://brilliant.org/wiki/floyd-warshall-algorithm/
- https://stackoverflow.com/questions/17480022/java-find-shortest-path-between-2-points-in-a-distance-weighted-map

*/

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method.
    *   @param args argument to use in main function
    */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();


   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 9,false);
      System.out.println (g);

      g.floydWarshall(); // calls the floydWarshall function


      Graph g2 = new Graph ("G2");
      g2.createRandomSimpleGraph (4, 4,true);
      System.out.println (g2);
      g2.floydWarshall(); // calls the floydWarshall function


   }


   /**
    * Task 24: Find two vertices that have the longest distance between them.
    *
    * Implementation:
    *
    * Added new variable mapMatrix in Graph class.
    * This matrix will have the value of the 'distance' between two vertices.
    *
    * Initialised mapMatrix in createRandomSimpleGraph function.
    *
    * mapMatrix is filled with INF (Infinity) as value for every element to start with
    * and the elements whcih have same row and colums have initial value of 0.
    *
    * This is as the distance of element to itself is 0,
    * and later the Infinity is is replaced with the actual value if a vertice is adjacent to another.
    *
    * Modified the arc function:
    * Whenever it creates new arc it also send that information to a new function 'createMap()' which updates
    * the mapMatrix and puts value of the element 1 at the row and columns of the vertices.
    *
    * This is because we consider every path (arc / edge) having equal weight (distance), in this case 1.
    *
    * Applying the Floyd - Warshall algorithm on the mapMatrix, it gives out the matrix with shortest paths between vertices.
    */

   static class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   static class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   }

   /**
    * Task 24: Find two vertices that have the longest distance between them.
    *
    * Modified methods: createRandomSimpleGraph, createArc
    * Added methods: makeMatrix, createMap, printMapMatrix, floydWarshall
    */
    static class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private int[][] mapMatrix; /** matrix of path costs (distance matrix) */
      final static int INF = 99999; /** used as the infinity value for the distance in the distance matrix*/
      private boolean random; /** used to choose if the distance matrix will have random weights or equal weights*/
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Used to make a new arc.
       * When makes the arc, also adds those vertices in the distance matrix by calling the 'createMap' function
       * @param aid ID of the arc
       * @param from the vertex from from where the arc starts
       * @param to the vertex where the arc goes
       * @return res the arc created
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         createMap(from,to);
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                       + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                       + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       * @param rand boolean to choose yes or no for random path-weights or no path weights respectively
       */
      public void createRandomSimpleGraph (int n, int m,boolean rand) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         this.mapMatrix = new int[n][n];
         this.makeMatrix(n);
         random = rand;

         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.

      /**
       * Used to fill the distance matrix.
       * By default filled with the Infinity 'INF' distance in all the cells, and '0' for where it is the same vertex in row and column
       * @param V number of vertices in the Graph
       */
      public void makeMatrix(int V){

         for (int i =0; i<V;i++){
            for (int j = 0;j<V;j++){
               this.mapMatrix[i][j] = INF;
               if (i == j){
                  this.mapMatrix[i][j] = 0;
               }
            }
         }
      }


      /**
       * Fill the distance matrix
       * The path cost depends on the boolean rand parameter in 'createRandomSimpleGraph':
       * if it was false, all path cost will be equal
       * if it was true, path cost will be decided randomly from range 1 - 10.
       * @param from vertex from where the path starts
       * @param to vertex to where path goes
       */
      public void createMap(Vertex from, Vertex to){
         int max = 10, min = 1;

         int fromVert = Integer.parseInt(from.toString().substring(1,2))-1;
         int toVert = Integer.parseInt(to.toString().substring(1,2))-1;

         if (fromVert != toVert) {
            if (random == false){
               this.mapMatrix[fromVert][toVert] = 1;
            }
            if (random == true){
               this.mapMatrix[fromVert][toVert] = (int)(Math.random() * ((max - min) + 1)) + min; //for having weights in paths
            }
         }
      }

      /**
       * Used to print the distance matrix. Not necessary for functionality, used for troubleshooting.
       */
      public void printMapMatrix(){

         int V = this.mapMatrix.length;

         for (int i = 0; i<V;i++){
            for (int j = 0; j<V; j++){
               System.out.print(this.mapMatrix[i][j]+" ");
            }
            System.out.println("");
         }
      }

      /**
       * This runs the Floyd-Warshall algorithm on the distance matrix,
       * and makes a new matrix with listing the shortest paths for each pair of vertices (called fwMatrix).
       * Then print that fwMatrix.
       * Additionally, also lists the longest distance between two vertices in a statement, after the matrix.
       */
      void floydWarshall(){

         int V = this.mapMatrix.length;

         int fwMatrix[][] = new int[V][V];
         int i, j, k;

         for (i = 0; i < V; i++) {
            for (j = 0; j < V; j++) {
               fwMatrix[i][j] = this.mapMatrix[i][j];
            }
         }

         for (k = 0; k < V; k++) {
            for (i = 0; i < V; i++) {
               for (j = 0; j < V; j++) {

                  if (fwMatrix[i][k] + fwMatrix[k][j] < fwMatrix[i][j]) {
                     fwMatrix[i][j] = fwMatrix[i][k] + fwMatrix[k][j];
                  }
               }
            }
         }

         // Printing the matrix
         int longest = 0;

         System.out.println("");
         System.out.println("After applying Floyd-Warshall algorithm, we get this matrix with the shortest path calculations: ");
         System.out.println("");
         System.out.print("     ");
         for (int a=1;a<=V;a++) {
            System.out.print("v" + a +"  ");
         }
         System.out.println("");
         System.out.print("----------------------------");
         System.out.println("");

         for (i=0; i<V; i++){
            System.out.print("v"+(i+1)+ " | ");
            for (j=0; j<V; j++) {

               if (fwMatrix[i][j] == INF){
                  System.out.print("INF ");
               }

               else {
                  System.out.print(fwMatrix[i][j]+"   ");
               }

               if (fwMatrix[i][j] > longest){
                  longest = fwMatrix[i][j];
               }
            }
            System.out.println();
         }
         System.out.print("----------------------------");
         System.out.println("");
         //System.out.println(""+longest);

         for (i=0; i<V; ++i) {
            for (j = 0; j < V; ++j) {
               if (fwMatrix[i][j] == longest){
                  System.out.println("Two vertices that have the longest distance between them: v"+(i+1)+" and v"+(j+1)+" . With distance: "+longest);
               }

            }
         }

      }


   }

}